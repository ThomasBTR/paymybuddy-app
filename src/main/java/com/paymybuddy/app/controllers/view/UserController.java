package com.paymybuddy.app.controllers.view;

import com.paymybuddy.app.controllers.utils.UsernameGetter;
import com.paymybuddy.app.database.models.BankAccount;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.services.BankAccountServices;
import com.paymybuddy.app.services.ContactServices;
import com.paymybuddy.app.services.UserServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ResolvableType;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * The type User controller.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    /**
     * The User services.
     */
    private final UserServices userServices;

    /**
     * The Contact services.
     */
    private final ContactServices contactServices;

    /**
     * The Bank account services.
     */
    private final BankAccountServices bankAccountServices;

    /**
     * .
     */
    private final UsernameGetter usernameGetter;

    /**
     * Instantiates a new User controller.
     *
     * @param userServices        the user services
     * @param contactServices     the contact services
     * @param bankAccountServices the bank account services
     * @param usernameGetter      the username getter
     */
    UserController(final UserServices userServices, final ContactServices contactServices, final BankAccountServices bankAccountServices, final UsernameGetter usernameGetter) {
        this.userServices = userServices;
        this.contactServices = contactServices;
        this.bankAccountServices = bankAccountServices;
        this.usernameGetter = usernameGetter;
    }


    /**
     * Home page.
     *
     * @return the string
     */
    @GetMapping("/index")
    public String userIndex() {
        return "user/index";
    }

    /**
     * Post signup string.
     *
     * @param username the username
     * @param password the password
     * @param model    the model
     * @return the string
     */
    @PostMapping("/signup")
    public String postSignup(@ModelAttribute("username") final String username, @ModelAttribute("password") final String password, final Model model) {
        userServices.createUser(username, password);
        model.addAttribute("signUp", true);
        return "login";
    }

    /**
     * Gets contact.
     *
     * @param authentication the authentication
     * @param model          the model
     * @return the contact
     */
    @GetMapping("/contact")
    public String getContact(final Authentication authentication, final Model model) {
        String userId = usernameGetter.usernameGetter(authentication);
        List<String> contactList = new ArrayList<>();
        Iterable<User> contacts = null;
        ResolvableType type = ResolvableType.forInstance(userServices.getAllUsers())
                .as(Iterable.class);
        if (type != ResolvableType.NONE) {
            contacts = contactServices.getAvailableContacts(userId);
        }
        assert contacts != null;
        contacts.forEach(contact ->
                contactList.add(contact.getId()));
        model.addAttribute("contacts", contactList);
        return "user/contact";
    }


    /**
     * Add contact string.
     *
     * @param authentication the authentication
     * @param model          the model
     * @param contactId      the contact id
     * @return the string
     */
    @PostMapping("/contact")
    public String addContact(final Authentication authentication, final Model model, @ModelAttribute("contactId") final String contactId) {
        String userId = usernameGetter.usernameGetter(authentication);
        contactServices.addContact(userId, contactId);
        model.addAttribute("addContact", true);
        return "redirect:transfer";
    }

    /**
     * Gets profile.
     *
     * @param authentication the authentication
     * @param model          the model
     * @return the profile
     */
    @GetMapping("/profile")
    public String getProfile(final Authentication authentication, final Model model) {
        String userId = usernameGetter.usernameGetter(authentication);
        User user = userServices.getUser(userId);
        model.addAttribute("userInfo", user);
        List<String> bankAccounts = new ArrayList<>();
        Iterable<BankAccount> bankAccountIterable = null;
        ResolvableType type = ResolvableType.forInstance(userServices.getAllUsers())
                .as(Iterable.class);
        if (type != ResolvableType.NONE) {
            bankAccountIterable = user.getBankAccounts();
        }
        assert bankAccountIterable != null;
        bankAccountIterable.forEach(bankAccount ->
                bankAccounts.add(bankAccount.getIban()));
        model.addAttribute("bankAccounts", bankAccounts);
        List<String> transferTypes = new ArrayList<>();
        transferTypes.add("Bank Account Credit");
        transferTypes.add("Bank Account Debit");
        model.addAttribute("transferTypes", transferTypes);
        return "user/profile";
    }

    /**
     * Add bank account string.
     *
     * @param authentication the authentication
     * @param model          the model
     * @param iban           the iban
     * @param bic            the bic
     * @return the string
     */
    @PostMapping("/profile/addBankAccount")
    public String addBankAccount(final Authentication authentication, final Model model, @ModelAttribute("iban") final String iban, @ModelAttribute("bic") final String bic) {
        String userId = usernameGetter.usernameGetter(authentication);
        try {
            bankAccountServices.addBankAccount(userId, iban, bic);
            model.addAttribute("AddBankAccountSuccess", true);
        }  catch (RuntimeException e)  {
            LOGGER.error(e.getMessage(), e);
            model.addAttribute("AddBankAccountError", true);
            model.addAttribute("AddBankAccountErrorMessage", e.getMessage());
        }

        return getProfile(authentication, model);
    }
}
