package com.paymybuddy.app.controllers.view;

import com.paymybuddy.app.controllers.utils.UsernameGetter;
import com.paymybuddy.app.database.models.BankAccount;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.services.BankAccountServices;
import com.paymybuddy.app.services.BankTransfertServices;
import com.paymybuddy.app.services.ContactServices;
import com.paymybuddy.app.services.PaymentServices;
import com.paymybuddy.app.services.UserServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Transfer controller.
 */
@Controller
@RequestMapping("/user")
public class TransferController {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(TransferController.class);

    /**
     * .
     */
    private final UserController userController;

    /**
     * The Contact services.
     */
    private final ContactServices contactServices;

    /**
     * The Payment services.
     */
    private final PaymentServices paymentServices;

    /**
     * The User services.
     */
    private final UserServices userServices;

    /**
     * .
     */
    private final BankTransfertServices bankTransfertServices;

    /**
     * .
     */
    private final BankAccountServices bankAccountServices;

    /**
     * .
     */
    private final UsernameGetter usernameGetter;

    /**
     * Instantiates a new Transfer controller.
     *
     * @param userController        the user controller
     * @param contactServices       the contact services
     * @param paymentServices       the payment services
     * @param userServices          the user services
     * @param bankTransfertServices the bank transfert services
     * @param bankAccountServices   the bank account services
     * @param usernameGetter        the username getter
     */
    public TransferController(final UserController userController, final ContactServices contactServices,
                              final PaymentServices paymentServices, final UserServices userServices,
                              final BankTransfertServices bankTransfertServices, final BankAccountServices bankAccountServices, final UsernameGetter usernameGetter) {
        this.userController = userController;
        this.contactServices = contactServices;
        this.paymentServices = paymentServices;
        this.userServices = userServices;
        this.bankTransfertServices = bankTransfertServices;
        this.bankAccountServices = bankAccountServices;
        this.usernameGetter = usernameGetter;
    }

    /**
     * Gets transfer.
     *
     * @param authentication the authentication
     * @param model          the model
     * @return the transfer
     */
    @GetMapping("/transfer")
    public String getTransfer(final Authentication authentication, final Model model) {
        List<String> contactList = new ArrayList<>();
        Iterable<User> contacts = new ArrayList<>();
        String userId = usernameGetter.usernameGetter(authentication);
        if (null != contactServices.getContacts(userId)) {
            contacts = contactServices.getContacts(userId);
        }
        contacts.forEach(contact ->
                contactList.add(contact.getId()));
        model.addAttribute("contacts", contactList);
        model.addAttribute("payments", paymentServices.getAllPayments(userId));
        return "user/transfer";
    }

    /**
     * Post transfer string.
     *
     * @param authentication the authentication
     * @param amount         the amount
     * @param contactId      the contact id
     * @param model          the model
     * @return the string
     */
    @PostMapping("/transfer")
    public String postTransfer(final Authentication authentication, @ModelAttribute("amount") final String amount, @ModelAttribute("contactId") final String contactId, final Model model) {
        String userId = usernameGetter.usernameGetter(authentication);
        User debtor = userServices.getUser(userId);
        User creditor = userServices.getUser(contactId);
        try {
            paymentServices.transfertMoney(Float.parseFloat(amount), creditor, debtor);
            model.addAttribute("transferSuccess", true);
        } catch (RuntimeException runtimeException) {
            LOGGER.error(runtimeException.getMessage(), runtimeException);
            model.addAttribute("transferError", true);
            model.addAttribute("transferErrorMessage", runtimeException.getMessage());
        }
        return getTransfer(authentication, model);
    }

    /**
     * .
     *
     * @param authentication the authentication
     * @param iban           the iban
     * @param transferType   the transfer type
     * @param amount         the amount
     * @param model          the model
     * @return string string
     */
    @PostMapping("/profile")
    public String postProfileBankAccount(final Authentication authentication, @ModelAttribute("bankAccount") final String iban, @ModelAttribute("transferType") final String transferType,
                                         @ModelAttribute("amount") final String amount, final Model model) {
        String userId = usernameGetter.usernameGetter(authentication);
        User owner = userServices.getUser(userId);
        BankAccount ba = bankAccountServices.getBankAccount(userId, iban);
        try {
            if (transferType.equals("Bank Account Credit")) {
                bankTransfertServices.transfertMoneyFromBalanceToBankAccount(Float.parseFloat(amount), owner, ba);
            } else if (transferType.equals("Bank Account Debit")) {
                bankTransfertServices.transfertMoneyFromBankToBalance(Float.parseFloat(amount), owner, ba);
            }
            model.addAttribute("transferSuccess", true);
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage(), e);
            model.addAttribute("transferError", true);
            model.addAttribute("transferErrorMessage", e.getMessage());
        }
        return userController.getProfile(authentication, model);
    }
}
