package com.paymybuddy.app.controllers.view;

import org.springframework.core.ResolvableType;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.HashMap;
import java.util.Map;

/**
 * Application home page and login.
 */
@Controller
public class MainController {

    /**
     * The constant OAUTH_2_AUTHORIZATION.
     */
    private static final String OAUTH_2_AUTHORIZATION = "oauth2/authorization";
    /**
     * The Oauth 2 authentication urls.
     */
    private final Map<String, String> oauth2AuthenticationUrls = new HashMap<>();

    /**
     * The Client registration repository.
     */
    private final ClientRegistrationRepository clientRegistrationRepository;

    /**
     * Instantiates a new Main controller.
     *
     * @param clientRegistrationRepository the client registration repository
     */
    public MainController(final ClientRegistrationRepository clientRegistrationRepository) {
        this.clientRegistrationRepository = clientRegistrationRepository;
    }

    /**
     * Home page.
     *
     * @return the string
     */
    @GetMapping("/")
    public String index() {
        return "index";
    }

    /**
     * Home page.
     *
     * @param model the model
     * @return the string
     */
    @GetMapping("/login")
    public String login(final Model model) {
        Iterable<ClientRegistration> clientRegistrations = null;
        ResolvableType type = ResolvableType.forInstance(clientRegistrationRepository)
                .as(Iterable.class);
        if (type != ResolvableType.NONE
                &&
                ClientRegistration.class.isAssignableFrom(type.resolveGenerics()[0])) {
            clientRegistrations = (Iterable<ClientRegistration>) clientRegistrationRepository;
        }

        assert clientRegistrations != null;
        clientRegistrations.forEach(registration ->
                oauth2AuthenticationUrls.put(registration.getClientName(),
                        OAUTH_2_AUTHORIZATION + "/" + registration.getRegistrationId()));
        model.addAttribute("urls", oauth2AuthenticationUrls);

        return "login";
    }

    /**
     * Login error string.
     *
     * @param model the model
     * @return the string
     */
// Login form with error
    @GetMapping("/login-error")
    public String loginError(final Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }

    /**
     * Gets signup.
     *
     * @param model the model
     * @return the signup
     */
//Signup form
    @GetMapping("/signup")
    public String getSignup(final Model model) {
        model.addAttribute("username");
        model.addAttribute("password");
        return "signup";
    }

}
