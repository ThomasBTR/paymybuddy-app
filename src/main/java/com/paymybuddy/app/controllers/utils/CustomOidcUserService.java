package com.paymybuddy.app.controllers.utils;

import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.database.repositories.UserRepository;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * The type Custom oidc user service.
 */
@Service
public class CustomOidcUserService extends OidcUserService {
    /**
     * The User repository.
     */
    private final UserRepository userRepository;

    /**
     * Instantiates a new Custom oidc user service.
     *
     * @param userRepository the user repository
     */
    public CustomOidcUserService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * .
     * @param userRequest The OidcUserRequest
     * @return An Oidc User
     * @throws OAuth2AuthenticationException in case of exception
     */
    @Override
    public OidcUser loadUser(final OidcUserRequest userRequest) throws OAuth2AuthenticationException {
        OidcUser oidcUser = super.loadUser(userRequest);

        try {
            return processOidcUser(oidcUser);
        } catch (Exception ex) {
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex.getCause());
        }
    }

    /**
     * Process oidc user oidc user.
     *
     * @param oidcUser    the oidc user
     * @return the oidc user
     */
    public OidcUser processOidcUser(final OidcUser oidcUser) {
        OidcUserInfo oidcUserInfo = new OidcUserInfo(oidcUser.getAttributes());

        // see what other data from userRequest or oidcUser you need

        Optional<User> userOptional = userRepository.findById(oidcUserInfo.getEmail());
        if (!userOptional.isPresent()) {
            User user = new User(oidcUserInfo.getEmail(), oidcUser.getName());
            // set other needed data

            userRepository.save(user);
        }

        return oidcUser;
    }
}
