package com.paymybuddy.app.controllers.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.stereotype.Component;

/**
 * The type Authentication facade.
 */
@Component
public class UsernameGetter {

    /**
     * Username getter string.
     *
     * @param authentication the authentication
     * @return the string
     */
    public String usernameGetter(final Authentication authentication) {
        String userId = null;
        if (authentication.getPrincipal().getClass() == DefaultOidcUser.class) {
            DefaultOAuth2User principal = (DefaultOAuth2User) authentication.getPrincipal();
            if (null != principal) {
                userId = principal.getAttribute("email");
            }
        } else {
            userId = authentication.getName();
        }
        return userId;
    }
}
