package com.paymybuddy.app.controllers.utils;

import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * The type O auth 2 user info.
 */
@Component
public class OidcUserInfo {
    /**
     * The Attributes.
     */
    private final Map<String, Object> attributes;

    /**
     * Instantiates a new Google user info.
     *
     * @param attributes the attributes
     */
    public OidcUserInfo(final Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return (String) attributes.get("sub");
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return (String) attributes.get("email");
    }
}
