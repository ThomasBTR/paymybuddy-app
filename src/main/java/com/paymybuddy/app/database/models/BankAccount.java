package com.paymybuddy.app.database.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The type Bank account.
 */
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "T_BANKACCOUNT")
public class BankAccount {

    /**
     * The Id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ba_id")
    private long id;

    /**
     * The Owner.
     */
    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "owner_id")
    private User owner;

    /**
     * The Bank transferts.
     */
    @OneToMany(
            mappedBy = "bankAccount",
            cascade = CascadeType.PERSIST,
            fetch = FetchType.LAZY
    )
    @Column(name = "bank_id")
    private List<BankTransfert> bankTransferts;

    /**
     * The Iban.
     */
    @Column(name = "iban")
    private String iban;

    /**
     * The Bic.
     */
    @Column(name = "bic")
    private String bic;


    /**
     * Instantiates a new Bank account.
     *
     * @param id   the id
     * @param user the user
     * @param iban the iban
     * @param bic  the bic
     */
    public BankAccount(final long id, final User user, final String iban, final String bic) {
        this.id = id;
        this.owner = user;
        this.iban = iban;
        this.bic = bic;
        this.bankTransferts = new ArrayList<>();
    }

    /**
     * .
     * @param o
     * @return
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BankAccount that = (BankAccount) o;

        if (id != that.id) return false;
        if (!Objects.equals(owner, that.owner)) return false;
        if (!Objects.equals(iban, that.iban)) return false;
        return Objects.equals(bic, that.bic);
    }

    /**
     * .
     * @return
     */
    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (bankTransferts != null ? bankTransferts.hashCode() : 0);
        result = 31 * result + (iban != null ? iban.hashCode() : 0);
        result = 31 * result + (bic != null ? bic.hashCode() : 0);
        return result;
    }

    /**
     * .
     * @return
     */
    @Override
    public String toString() {
        return "BankAccount{" +
                "id=" + id +
                ", owner=" + owner +
                ", iban='" + iban + '\'' +
                ", bic='" + bic + '\'' +
                '}';
    }
}
