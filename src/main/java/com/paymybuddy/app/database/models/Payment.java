package com.paymybuddy.app.database.models;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * The type Payment.
 */
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "T_PAYMENT")
public class Payment implements Serializable {


    /**
     * The Id.
     */
    @Id
    @Column(name = "p_id")
    private long id;

    /**
     * The Creditor.
     */
    @ManyToOne
    @JoinColumn(name = "creditor_id")
    private User creditor;

    /**
     * The Debtor.
     */
    @ManyToOne
    @JoinColumn(name = "debtor_id")
    private User debtor;

    /**
     * The Amount.
     */
    @Column(name = "amount")
    private float amount;

    /**
     * The Fees.
     */
    @Column(name = "fees")
    private float fees;

    /**
     * The Date.
     */
    @Column(name = "date")
    private OffsetDateTime date;

    /**
     * The Message.
     */
    @Column(name = "message")
    private String message;

    /**
     * .
     * @param o
     * @return
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Payment payment = (Payment) o;

        if (id != payment.id) return false;
        if (Float.compare(payment.amount, amount) != 0) return false;
        if (!creditor.equals(payment.creditor)) return false;
        if (!debtor.equals(payment.debtor)) return false;
        return Objects.equals(date, payment.date);
    }

    /**
     * .
     * @return
     */
    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + creditor.hashCode();
        result = 31 * result + debtor.hashCode();
        result = 31 * result + (amount != +0.0f ? Float.floatToIntBits(amount) : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }
}
