package com.paymybuddy.app.database.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

/**
 * The type Contact primary key.
 */
@Embeddable
@Getter
@Setter
public class ContactPrimaryKey implements Serializable {

    /**
     * The constant serialVersionUID.
     */
    private static final long serialVersionUID = -2888489701861649983L;

    /**
     * The Contactor id.
     */
    @ManyToOne
    @JoinColumn(name = "contactor_id")
    private User contactorId;


    /**
     * The Contacted id.
     */
    @ManyToOne
    @JoinColumn(name = "contacted_id")
    private User contactedId;

    /**
     * .
     * @param o
     * @return
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContactPrimaryKey that = (ContactPrimaryKey) o;

        if (!Objects.equals(contactorId, that.contactorId)) return false;
        return Objects.equals(contactedId, that.contactedId);
    }

    /**
     * .
     * @return
     */
    @Override
    public int hashCode() {
        int result = contactorId != null ? contactorId.hashCode() : 0;
        result = 31 * result + (contactedId != null ? contactedId.hashCode() : 0);
        return result;
    }
}
