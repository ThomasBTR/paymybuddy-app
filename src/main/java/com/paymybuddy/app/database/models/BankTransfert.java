package com.paymybuddy.app.database.models;

import com.paymybuddy.app.database.models.constants.TransfertType;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * The type Bank transfert.
 */
@Entity
@Getter
@Setter
@RequiredArgsConstructor
@Table(name = "T_BANK_TRANSFERT")
public class BankTransfert {

    /**
     * The Id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    /**
     * The Transfert type.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "transfert_type")
    private TransfertType transfertType;

    /**
     * The Amount.
     */
    @Column(name = "amount")
    private float amount;

    /**
     * The Date.
     */
    @Column(name = "date")
    private OffsetDateTime date;

    /**
     * The Owner id.
     */
    @ManyToOne
    @JoinColumn(name = "transaction_owner")
    private User ownerId;

    /**
     * The Bank account.
     */
    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "bank_id")
    private BankAccount bankAccount;

    /**
     * .
     * @return
     */
    @Override
    public String toString() {
        return "BankTransfert{" +
                "id=" + id +
                ", transfertType=" + transfertType +
                ", amount=" + amount +
                ", date=" + date +
                ", ownerId=" + ownerId +
                ", bankAccount=" + bankAccount +
                '}';
    }

    /**
     * .
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BankTransfert that = (BankTransfert) o;

        if (id != that.id) return false;
        if (Float.compare(that.amount, amount) != 0) return false;
        if (transfertType != that.transfertType) return false;
        if (!Objects.equals(date, that.date)) return false;
        if (!Objects.equals(ownerId, that.ownerId)) return false;
        return Objects.equals(bankAccount, that.bankAccount);
    }

    /**
     * .
     * @return
     */
    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (transfertType != null ? transfertType.hashCode() : 0);
        result = 31 * result + (amount != +0.0f ? Float.floatToIntBits(amount) : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (ownerId != null ? ownerId.hashCode() : 0);
        result = 31 * result + (bankAccount != null ? bankAccount.hashCode() : 0);
        return result;
    }
}
