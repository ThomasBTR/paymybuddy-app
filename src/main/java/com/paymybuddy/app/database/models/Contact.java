package com.paymybuddy.app.database.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * The type Contact.
 */
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "T_CONTACT")
public class Contact implements Serializable {

    /**
     * The Id.
     */
    @EmbeddedId
    private ContactPrimaryKey id;

    /**
     * .
     * @param o
     * @return
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contact contact = (Contact) o;

        return Objects.equals(id, contact.id);
    }

    /**
     * .
     * @return
     */
    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
