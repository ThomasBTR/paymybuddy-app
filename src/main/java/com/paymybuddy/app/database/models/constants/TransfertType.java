package com.paymybuddy.app.database.models.constants;

/**
 * The enum Transfert type.
 */
public enum TransfertType {
    /**
     * Debitbank transfert type.
     */
    DEBITBANK,
    /**
     * Creditbank transfert type.
     */
    CREDITBANK
}
