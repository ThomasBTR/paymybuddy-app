package com.paymybuddy.app.database.models;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The type User.
 */
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "T_USER")
public class User implements Serializable {

    /**
     * The Id.
     */
    @Id
    @Column(name = "id")
    private String id;

    /**
     * The Password.
     */
    @NotNull
    @Column(name = "password")
    private String password;

    /**
     * The Balance.
     */
    @Column(name = "balance")
    private Float balance;

    /**
     * The Bank accounts.
     */
    @OneToMany
            (mappedBy = "owner",
                    cascade = CascadeType.PERSIST,
                    fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<BankAccount> bankAccounts;

    /**
     * The Bank transferts.
     */
    @OneToMany
            (
                    mappedBy = "ownerId",
                    cascade = CascadeType.PERSIST,
                    fetch = FetchType.LAZY
            )
    @Column(name = "transaction_owner")
    @ToString.Exclude
    private List<BankTransfert> bankTransferts;

    /**
     * The Creditor payments.
     */
    @OneToMany
            (mappedBy = "creditor",
                    cascade = CascadeType.ALL,
                    fetch = FetchType.LAZY)
    @Column(name = "creditor_id")
    @ToString.Exclude
    private List<Payment> creditorPayments;

    /**
     * The Debtor payments.
     */
    @OneToMany
            (mappedBy = "debtor",
                    cascade = CascadeType.ALL,
                    fetch = FetchType.EAGER)
    @Column(name = "debtor_id")
    @ToString.Exclude
    private List<Payment> debtorPayments;

    /**
     * The Contacts.
     */
    @ManyToMany(
            fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.REMOVE
            })
    @JoinTable
            (name = "T_CONTACT",
                    joinColumns = @JoinColumn(name = "contactor_id"),
                    inverseJoinColumns = @JoinColumn(name = "contacted_id"))
    @ToString.Exclude
    private List<User> contacts;


    /**
     * The Authority.
     */
    @Column(name = "authority")
    private String authority;

    /**
     * Instantiates a new User.
     *
     * @param id       the id
     * @param password the password
     */
    public User(final String id, final String password) {
        this.id = id;
        this.password = password;
        this.contacts = new ArrayList<>();
        this.balance = 0F;
        this.bankAccounts = new ArrayList<>();
        this.creditorPayments = new ArrayList<>();
        this.debtorPayments = new ArrayList<>();
        this.bankTransferts = new ArrayList<>();
        this.authority = "ROLE_USER";
    }

    /**
     * .
     * @param o
     * @return
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        User user = (User) o;
        return id != null && Objects.equals(id, user.id);
    }

    /**
     * .
     * @return
     */
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
