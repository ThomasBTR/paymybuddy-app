package com.paymybuddy.app.database.repositories;

import com.paymybuddy.app.database.models.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The interface Bank account repository.
 */
@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {

    /**
     * .
     * @param iban
     * @return
     */
    @Query("SELECT ba FROM BankAccount ba WHERE ba.iban LIKE %:variable%")
    List<BankAccount> getBankAccountsByIban(@Param("variable")String iban);


    /**
     * Gets all payments.
     *
     * @return the all payments
     */
    @Query("SELECT ba FROM BankAccount ba")
    List<BankAccount> getAllBankAccounts();

}
