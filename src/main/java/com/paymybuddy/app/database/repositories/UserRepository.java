package com.paymybuddy.app.database.repositories;

import com.paymybuddy.app.database.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The interface User repository.
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {


    /**
     * Find all user list.
     *
     * @return the list
     */
    @Query("SELECT u from User u")
	List<User> findAllUser();
}
