package com.paymybuddy.app.database.repositories;

import com.paymybuddy.app.database.models.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The interface Payment repository.
 */
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {

    /**
     * Gets all payments.
     *
     * @return the all payments
     */
    @Query("SELECT p FROM Payment p")
    List<Payment> getAllPayments();

    /**
     * Gets payment by creditor.
     *
     * @param debtorId the debtor id
     * @return the payment by creditor
     */
    @Query("SELECT p FROM Payment p WHERE p.debtor.id LIKE %:variable%")
    List<Payment> getPaymentByCreditor(@Param("variable")String debtorId);

}
