package com.paymybuddy.app.database.repositories;

import com.paymybuddy.app.database.models.BankTransfert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The interface Bank transfert repository.
 */
@Repository
public interface BankTransfertRepository extends JpaRepository<BankTransfert, Long> {

    /**
     * .
     * @return
     */
    @Query("SELECT bt FROM BankTransfert bt")
    List<BankTransfert> getAllBankTransfers();

}
