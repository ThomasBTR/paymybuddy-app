package com.paymybuddy.app.database.repositories;

import com.paymybuddy.app.database.models.Contact;
import com.paymybuddy.app.database.models.ContactPrimaryKey;
import com.paymybuddy.app.database.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The interface Contact repository.
 */
@Repository
public interface ContactRepository extends JpaRepository<Contact, ContactPrimaryKey> {

    /**
     * Find by id contacted id list.
     *
     * @param contactedId the contacted id
     * @return the list
     */
    List<Contact> findByIdContactedId(User contactedId);

    /**
     * Find by id contactor id list.
     *
     * @param contactorId the contactor id
     * @return the list
     */
    List<Contact> findByIdContactorId(User contactorId);
}
