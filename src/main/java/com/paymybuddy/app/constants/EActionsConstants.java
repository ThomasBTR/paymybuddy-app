package com.paymybuddy.app.constants;

/**
 * The enum E actions constants.
 */
public enum EActionsConstants {
    /**
     * The User create start.
     */
    USER_CREATE_START("Started create a new user with id {}"),
    /**
     * The User create succeed.
     */
    USER_CREATE_SUCCEED("User with id {} create successfully"),
    /**
     * The User create error.
     */
    USER_CREATE_ERROR("id {} already used"),
    /**
     * The User connection start.
     */
    USER_CONNECTION_START("User with id {} try to connect"),
    /**
     * The User connection succeed.
     */
    USER_CONNECTION_SUCCEED("User with id {} succeed connection"),
    /**
     * The User get contact start.
     */
    USER_GET_CONTACT_START("Start to get user contacts for id {}"),
    /**
     * The User get contact succeed.
     */
    USER_GET_CONTACT_SUCCEED("User with id {} contacts found"),
    /**
     * The User get contact error.
     */
    USER_GET_CONTACT_ERROR("User with id {} contacts not found"),
    /**
     * The User get available contact start.
     */
    USER_GET_AVAILABLE_CONTACT_START("Start to get user contacts for id {}"),
    /**
     * The User get available contact succeed.
     */
    USER_GET_AVAILABLE_CONTACT_SUCCEED("User with id {} contacts found"),
    /**
     * The User get available contact error.
     */
    USER_GET_AVAILABLE_CONTACT_ERROR("User with id {} contacts not found"),
    /**
     * The User add contact start.
     */
    USER_ADD_CONTACT_START("Start to add id {} to user {} contact list"),
    /**
     * The User add contact succeed.
     */
    USER_ADD_CONTACT_SUCCEED("User with id {} added {} to his contact list"),
    /**
     * The User get contacts start.
     */
    USER_GET_CONTACTS_START("Start to get contacts list from user id {}"),
    /**
     * The User get contacts succeed.
     */
    USER_GET_CONTACTS_SUCCEED("Successfully get contacts list from user id {}"),
    /**
     * The Bank account add start.
     */
    BANK_ACCOUNT_ADD_START("Adding bank account to {} id"),
    /**
     * The Transaction between users start.
     */
    TRANSACTION_BETWEEN_USERS_START("Started a transfert between users {} and {}"),
    /**
     * The Transaction balance to bank account start.
     */
    TRANSACTION_BALANCE_TO_BANK_ACCOUNT_START("Started a transfert between owner {} balance to his bank account"),
    /**
     * The Transaction balance to bank account succeed.
     */
    TRANSACTION_BALANCE_TO_BANK_ACCOUNT_SUCCEED("Succeed a transfert between owner {} balance to his bank account"),
    /**
     * The Transaction error negative amount.
     */
    TRANSACTION_ERROR_NEGATIVE_AMOUNT("Your amount should be positive and greater than 0"),
    /**
     * The Transaction balance error amount too high.
     */
    TRANSACTION_BALANCE_ERROR_AMOUNT_TOO_HIGH("Transfert impossible : your balance is lower than the amount you want to transfert"),
    /**
     * The Transaction bank to balance account start.
     */
    TRANSACTION_BANK_TO_BALANCE_ACCOUNT_START("Stated a transfert between owner {} bank account and his balance"),
    /**
     * The Invalid length.
     */
    INVALID_LENGTH("IBAN and/or BIC as an invalid length"),
    /**
     * The Password invalid.
     */
    PASSWORD_INVALID("The password {} is incorrect. Please try again"),
    /**
     * The Unknown user.
     */
    UNKNOWN_USER("This user is not register in the database");

    /**
     * The Value.
     */
    private final String value;

    /**
     * Instantiates a new E actions constants.
     *
     * @param value the value
     */
    EActionsConstants(final String value){
        this.value = value;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * .
     * @return
     */
    @Override
    public String toString() {
        return "EErrorConstants{" +
                "value='" + value + '\'' +
                '}';
    }
}
