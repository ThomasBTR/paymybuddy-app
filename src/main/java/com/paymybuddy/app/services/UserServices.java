package com.paymybuddy.app.services;

import com.paymybuddy.app.configuration.SpringSecurityConfig;
import com.paymybuddy.app.constants.EActionsConstants;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.database.repositories.UserRepository;
import com.paymybuddy.app.exceptions.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * The type User services.
 */
@Service
public class UserServices {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServices.class);

    /**
     * The User repository.
     */
    private final UserRepository userRepository;

    /**
     * The Spring security config.
     */
    private final SpringSecurityConfig springSecurityConfig;

    /**
     * Instantiates a new User services.
     *
     * @param userRepository       the user repository
     * @param springSecurityConfig the spring security config
     */
    public UserServices(final UserRepository userRepository, final SpringSecurityConfig springSecurityConfig) {
        this.userRepository = userRepository;
        this.springSecurityConfig = springSecurityConfig;
    }

    /**
     * Create user user.
     *
     * @param id       the id
     * @param password the password
     * @return the user
     */
    @Transactional
    public User createUser(final String id, final String password) {
        User user = null;
        Optional<User> verify = userRepository.findById(id);
        if (verify.isPresent()) {
            LOGGER.warn(EActionsConstants.USER_CREATE_ERROR.getValue(), id);
            //throw exception.
        } else {
            user = new User(id, springSecurityConfig.passwordEncoder().encode(password));
            user = userRepository.save(user);
        }
        return user;
    }

    /**
     * Connect with user boolean.
     *
     * @param id       the id
     * @param password the password
     * @return the boolean
     */
    @Transactional
    public boolean connectWithUser(final String id, final String password) {
        LOGGER.debug(EActionsConstants.USER_CONNECTION_START.getValue(), id);
        boolean response = false;
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            if (password.equals(user.get().getPassword())) {
                LOGGER.info(EActionsConstants.USER_CONNECTION_SUCCEED.getValue(), id);
                response = true;
            } else {
                LOGGER.warn(EActionsConstants.PASSWORD_INVALID.getValue(), password);
            }
        } else {
            LOGGER.warn(EActionsConstants.UNKNOWN_USER.getValue());
        }
        return response;
    }

    /**
     * Get user user.
     *
     * @param id the id
     * @return the user
     */
    @Transactional
    public User getUser(final String id) {
        return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(String.format(EActionsConstants.USER_GET_CONTACT_ERROR.toString(), id)));
    }

    /**
     * Get all users list.
     *
     * @return the list
     */
    @Transactional
    public List<User>  getAllUsers() {
        return userRepository.findAllUser();
    }


    /**
     * Save user.
     *
     * @param user the user
     * @return the user
     */
    @Transactional
    public User save(final User user){
        return userRepository.save(user);
    }
}
