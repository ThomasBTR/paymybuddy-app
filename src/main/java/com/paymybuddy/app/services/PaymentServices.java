package com.paymybuddy.app.services;

import com.paymybuddy.app.constants.EActionsConstants;
import com.paymybuddy.app.database.models.Payment;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.database.repositories.PaymentRepository;
import com.paymybuddy.app.database.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.List;

/**
 * The type Payment services.
 */
@Service
public class PaymentServices {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentServices.class);

    /**
     * The User repository.
     */
    private final UserRepository userRepository;

    /**
     * The Payment repository.
     */
    private final PaymentRepository paymentRepository;

    /**
     * The Creditor.
     */
    static final String CREDITOR = "creditor";

    /**
     * The Debtor.
     */
    static final String DEBTOR = "debtor";

    /**
     * The Fees.
     */
    @Value("${payment.fees}")
    private String fees;


    /**
     * Instantiates a new Payment services.
     *
     * @param userRepository    the user repository
     * @param paymentRepository the payment repository
     */
    public PaymentServices(final UserRepository userRepository, final PaymentRepository paymentRepository) {
        this.userRepository = userRepository;
        this.paymentRepository = paymentRepository;
    }

    /**
     * Do transfert payment.
     *
     * @param amount   the amount
     * @param creditor the creditor
     * @param debtor   the debtor
     * @return the payment
     */
    private Payment doTransfert(final Float amount, final User creditor, final User debtor) {
        Payment payment = new Payment();
        payment.setId(getNextId());
        payment.setCreditor(creditor);
        payment.setDebtor(debtor);
        payment.setAmount(amount);
        payment.setFees(amount * Float.parseFloat(fees));
        payment.setDate(OffsetDateTime.now());
        payment = paymentRepository.save(payment);
        saveUser(creditor, payment, CREDITOR);
        saveUser(debtor, payment, DEBTOR);

        return payment;
    }

    /**
     * Get next id long.
     *
     * @return the long
     */
    private long getNextId(){
        List<Payment> payments = paymentRepository.getAllPayments();
        return Integer.toUnsignedLong(payments.size());
    }

    /**
     * Save user.
     *
     * @param user    the user
     * @param payment the payment
     * @param type    the type
     */
    private void saveUser(final User user, final Payment payment, final String type) {
        if (type.equals(CREDITOR)) {
            user.getCreditorPayments().add(payment);
            user.setBalance(user.getBalance() + payment.getAmount());
        } else if (type.equals(DEBTOR)) {
            user.getDebtorPayments().add(payment);
            user.setBalance(user.getBalance() - payment.getAmount() - payment.getFees());
        }
        userRepository.save(user);
    }

    /**
     * Transfert money payment.
     *
     * @param amount   the amount
     * @param creditor the creditor
     * @param debtor   the debtor
     * @return the payment
     */
    @Transactional
    public Payment transfertMoney(final Float amount, final User creditor, final User debtor) throws RuntimeException {
        LOGGER.debug(EActionsConstants.TRANSACTION_BETWEEN_USERS_START.getValue(), creditor.getId(), debtor.getId());
        Payment payment = null;

        float amountWithFees = amount + amount * Float.parseFloat(fees);

        if (amount > 0F && amountWithFees <= debtor.getBalance()) {
            payment = doTransfert(amount, creditor, debtor);
        } else if (amountWithFees <= 0F) {
            LOGGER.error(EActionsConstants.TRANSACTION_ERROR_NEGATIVE_AMOUNT.getValue());
            throw new RuntimeException(EActionsConstants.TRANSACTION_ERROR_NEGATIVE_AMOUNT.getValue());
        } else if (amountWithFees > debtor.getBalance()) {
            LOGGER.error(EActionsConstants.TRANSACTION_BALANCE_ERROR_AMOUNT_TOO_HIGH.getValue());
            throw new RuntimeException(EActionsConstants.TRANSACTION_BALANCE_ERROR_AMOUNT_TOO_HIGH.getValue());
        }
        return payment;
    }

    /**
     * Gets all payments.
     *
     * @param debtorId the debtor id
     * @return the all payments
     */
    @Transactional
    public List<Payment> getAllPayments(final String debtorId) {
        return paymentRepository.getPaymentByCreditor(debtorId);
    }
}
