package com.paymybuddy.app.services;

import com.paymybuddy.app.constants.EActionsConstants;
import com.paymybuddy.app.database.models.BankAccount;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.database.repositories.BankAccountRepository;
import com.paymybuddy.app.database.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * The type Bank account services.
 */
@Service
public class BankAccountServices {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(BankAccountServices.class);

    /**
     * The Bank account repository.
     */
    private final BankAccountRepository bankAccountRepository;

    /**
     * The User repository.
     */
    private final UserRepository userRepository;

    /**
     * The Iban length.
     */
    @Value("${iban.length}")
    private String ibanLength;


    /**
     * The Bic max length.
     */
    @Value("${bic.maxLength}")
    private String bicMaxLength;


    /**
     * The Bic min length.
     */
    @Value("${bic.minLength}")
    private String bicMinLength;

    /**
     * Instantiates a new Bank account services.
     *
     * @param userRepository        the user repository
     * @param bankAccountRepository the bank account repository
     */
    public BankAccountServices(final UserRepository userRepository, final BankAccountRepository bankAccountRepository) {
        this.bankAccountRepository = bankAccountRepository;
        this.userRepository = userRepository;
    }

    /**
     * Add bank account user.
     *
     * @param userId the user id
     * @param iban   the iban
     * @param bic    the bic
     * @return the user
     */
    @Transactional
    public User addBankAccount(final String userId, final String iban, final String bic) throws RuntimeException {
        LOGGER.debug(EActionsConstants.BANK_ACCOUNT_ADD_START.getValue(), userId);
        User userWithBankAccount = null;
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isPresent()) {
            userWithBankAccount = userOptional.get();
        }
        if (iban.length() == Integer.parseInt(ibanLength) && (bic.length() == Integer.parseInt(bicMaxLength) || bic.length() == Integer.parseInt(bicMinLength))) {
            BankAccount ba = new BankAccount(getNextId(), userWithBankAccount, iban, bic);
            bankAccountRepository.save(ba);
            Objects.requireNonNull(userWithBankAccount).getBankAccounts().add(ba);
            userWithBankAccount = userRepository.save(userWithBankAccount);
        } else {
            LOGGER.warn(EActionsConstants.INVALID_LENGTH.getValue());
            throw new RuntimeException(EActionsConstants.INVALID_LENGTH.getValue());
        }
        return userWithBankAccount;
    }

    /**
     * .
     *
     * @param userId the user id
     * @param iban   the iban
     * @return bank account
     */
    @Transactional
    public BankAccount getBankAccount(final String userId, final String iban) {
        BankAccount baReturned = null;
        User owner = userRepository.getById(userId);
        List<BankAccount> bankAccountsFromBdd = bankAccountRepository.getBankAccountsByIban(iban);
        for (BankAccount ba
                :
             bankAccountsFromBdd) {
            if (ba.getOwner().equals(owner)) {
                baReturned = ba;
            }
        }
        return baReturned;
    }

    /**
     * Get next id long.
     *
     * @return the long
     */
    private long getNextId() {
        List<BankAccount> allBankAccounts = bankAccountRepository.getAllBankAccounts();
        return Integer.toUnsignedLong(allBankAccounts.size());
    }
}
