package com.paymybuddy.app.services;

import com.paymybuddy.app.constants.EActionsConstants;
import com.paymybuddy.app.database.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Contact services.
 */
@Service
public class ContactServices {
    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ContactServices.class);

    /**
     * The User services.
     */
    private final UserServices userServices;

    /**
     * Instantiates a new Contact services.
     *
     * @param userServices      the user services
     */
    public ContactServices(final UserServices userServices) {
        this.userServices = userServices;
    }

    /**
     * Add contact user.
     *
     * @param userId    the user id
     * @param idContact the id contact
     * @return the user
     */
    @Transactional
    public User addContact(final String userId, final String idContact) {
        LOGGER.debug(EActionsConstants.USER_ADD_CONTACT_START.getValue(), idContact, userId);
        User user = userServices.getUser(userId);
        User contactUser = userServices.getUser(idContact);
        user.getContacts().add(contactUser);
        contactUser.getContacts().add(user);
        user = userServices.save(user);
        userServices.save(contactUser);
        LOGGER.info(EActionsConstants.USER_ADD_CONTACT_SUCCEED.getValue(), user.getId(), contactUser.getId());
        return user;
    }

    /**
     * Gets contacts.
     *
     * @param userId the user id
     * @return the contacts
     */
    @Transactional
    public List<User> getContacts(final String userId) {
        LOGGER.debug(EActionsConstants.USER_ADD_CONTACT_START.getValue(), userId);
        User buffer = userServices.getUser(userId);
        return buffer.getContacts();
    }

    /**
     * Gets available contacts.
     *
     * @param userId the user id
     * @return the available contacts
     */
    @Transactional
    public List<User> getAvailableContacts(final String userId) {
        LOGGER.debug(EActionsConstants.USER_GET_AVAILABLE_CONTACT_START.getValue(), userId);
        List<User> contacts = getContacts(userId);
        List<User> availableUsers = userServices.getAllUsers();
        List<User> availableNewContacts = new ArrayList<>();
        for (User user :
                availableUsers) {
            if (!user.getId().equals(userId) && !contacts.contains(user)) {
                availableNewContacts.add(user);
            }
        }
        return availableNewContacts;
    }
}
