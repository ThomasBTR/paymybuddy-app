package com.paymybuddy.app.services;

import com.paymybuddy.app.constants.EActionsConstants;
import com.paymybuddy.app.database.models.BankAccount;
import com.paymybuddy.app.database.models.BankTransfert;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.database.models.constants.TransfertType;
import com.paymybuddy.app.database.repositories.BankAccountRepository;
import com.paymybuddy.app.database.repositories.BankTransfertRepository;
import com.paymybuddy.app.database.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.List;

/**
 * The type Bank transfert services.
 */
@Service
public class BankTransfertServices {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(BankTransfertServices.class);

    /**
     * The User repository.
     */
    private final UserRepository userRepository;

    /**
     * The Bank transfert repository.
     */
    private final BankTransfertRepository bankTransfertRepository;

    /**
     * The Bank account repository.
     */
    private final BankAccountRepository bankAccountRepository;

    /**
     * .
     */
    @Value("${payment.fees}")
    private String fees;

    /**
     * Instantiates a new Bank transfert services.
     *
     * @param userRepository          the user repository
     * @param bankTransfertRepository the bank transfert repository
     * @param bankAccountRepository   the bank account repository
     */
    public BankTransfertServices(final UserRepository userRepository, final BankTransfertRepository bankTransfertRepository, final BankAccountRepository bankAccountRepository) {
        this.userRepository = userRepository;
        this.bankTransfertRepository = bankTransfertRepository;
        this.bankAccountRepository = bankAccountRepository;
    }


    /**
     * Do transfert bank transfert.
     *
     * @param amount        the amount
     * @param owner         the owner
     * @param transfertType the transfert type
     * @param bankAccount   the bank account
     * @return the bank transfert
     */
    private BankTransfert doTransfert(final Float amount, final User owner, final TransfertType transfertType, final BankAccount bankAccount) {
        BankAccount ba = bankAccount;
        BankTransfert bankTransfert = new BankTransfert();
        bankTransfert.setId(getNextId());
        bankTransfert.setTransfertType(transfertType);
        bankTransfert.setOwnerId(owner);
        bankTransfert.setAmount(amount);
        bankTransfert.setBankAccount(bankAccount);
        bankTransfert.setDate(OffsetDateTime.now());
        bankTransfert = bankTransfertRepository.save(bankTransfert);
        ba = saveBankAccount(ba, bankTransfert);
        saveOwner(owner, bankTransfert, ba);

        return bankTransfert;
    }

    private long getNextId() {
        List<BankTransfert> bankTransferts = bankTransfertRepository.getAllBankTransfers();
        return Integer.toUnsignedLong(bankTransferts.size());
    }

    /**
     * Save owner.
     *
     * @param owner         the owner
     * @param bankTransfert the bank transfer
     * @param bankAccount   the bank account
     */
    private void saveOwner(final User owner, final BankTransfert bankTransfert, final BankAccount bankAccount) {
        if (bankTransfert.getTransfertType().equals(TransfertType.CREDITBANK)) {
            owner.setBalance(owner.getBalance() - bankTransfert.getAmount() - bankTransfert.getAmount() * Float.parseFloat(fees));
        } else if (bankTransfert.getTransfertType().equals(TransfertType.DEBITBANK)) {
            owner.setBalance(owner.getBalance() + bankTransfert.getAmount() - bankTransfert.getAmount() * Float.parseFloat(fees));
        }
        owner.getBankTransferts().add(bankTransfert);
        owner.getBankAccounts().set(owner.getBankAccounts().indexOf(bankAccount), bankAccount);
        userRepository.save(owner);
    }

    /**
     * Save bank account bank account.
     *
     * @param bankAccount   the bank account
     * @param bankTransfert the bank transfert
     * @return the bank account
     */
    private BankAccount saveBankAccount(final BankAccount bankAccount, final BankTransfert bankTransfert) {
        bankAccount.getBankTransferts().add(bankTransfert);
        return bankAccountRepository.save(bankAccount);
    }

    /**
     * Transfert money from balance to bank account bank transfert.
     *
     * @param amount      the amount
     * @param owner       the owner
     * @param bankAccount the bank account
     * @return the bank transfert
     */
    @Transactional
    public BankTransfert transfertMoneyFromBalanceToBankAccount(final Float amount, final User owner, final BankAccount bankAccount)
            throws RuntimeException {
        LOGGER.debug(EActionsConstants.TRANSACTION_BALANCE_TO_BANK_ACCOUNT_START.getValue(), owner.getId());
        BankTransfert bankTransfert = null;

        float amountWithFees = amount + amount * Float.parseFloat(fees);

        if (amountWithFees > 0F && amountWithFees <= owner.getBalance()) {
            bankTransfert = doTransfert(amount, owner, TransfertType.CREDITBANK, bankAccount);
        } else if (amount <= 0F) {
            LOGGER.error(EActionsConstants.TRANSACTION_ERROR_NEGATIVE_AMOUNT.getValue());
            throw new RuntimeException(EActionsConstants.TRANSACTION_ERROR_NEGATIVE_AMOUNT.getValue());
        } else if (amount > owner.getBalance()) {
            LOGGER.error(EActionsConstants.TRANSACTION_BALANCE_ERROR_AMOUNT_TOO_HIGH.getValue());
            throw new RuntimeException(EActionsConstants.TRANSACTION_BALANCE_ERROR_AMOUNT_TOO_HIGH.getValue());
        }
        return bankTransfert;
    }


    /**
     * Transfert money from bank to balance bank transfert.
     *
     * @param amount      the amount
     * @param owner       the owner
     * @param bankAccount the bank account
     * @return the bank transfert
     */
    @Transactional
    public BankTransfert transfertMoneyFromBankToBalance(final Float amount, final User owner, final BankAccount bankAccount) throws RuntimeException {
        LOGGER.debug(EActionsConstants.TRANSACTION_BANK_TO_BALANCE_ACCOUNT_START.getValue(), owner.getId());
        BankTransfert bankTransfert = null;

        if (amount > 0F) {
            bankTransfert = doTransfert(amount, owner, TransfertType.DEBITBANK, bankAccount);
        } else if (amount <= 0F) {
            LOGGER.error(EActionsConstants.TRANSACTION_ERROR_NEGATIVE_AMOUNT.getValue());
            throw new RuntimeException(EActionsConstants.TRANSACTION_ERROR_NEGATIVE_AMOUNT.getValue());
        }
        return bankTransfert;
    }

}
