CREATE TABLE T_USER
(
    id       VARCHAR(50),
    password VARCHAR(255) NOT NULL,
    balance  FLOAT       NOT NULL,
    authority VARCHAR(50) NOT NULL,
    enabled TINYINT NOT NULL DEFAULT 1,
    PRIMARY KEY (id)
);

CREATE TABLE T_BANKACCOUNT
(
    ba_id    INTEGER,
    owner_id VARCHAR(45) NOT NULL,
    iban     VARCHAR(34) NOT NULL,
    bic      VARCHAR(11) NOT NULL,
    PRIMARY KEY (ba_id),
    FOREIGN KEY (owner_id) REFERENCES T_USER (id) ON DELETE RESTRICT
);

CREATE TABLE T_PAYMENT
(
    p_id        INTEGER,
    creditor_id VARCHAR(45) NOT NULL,
    debtor_id   VARCHAR(45) NOT NULL,
    amount      FLOAT       NOT NULL,
    fees        FlOAT       NOT NULL,
    date        TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    message     VARCHAR(254),
    PRIMARY KEY (p_id),
    FOREIGN KEY (creditor_id) REFERENCES T_USER (id) ON DELETE RESTRICT,
    FOREIGN KEY (debtor_id) REFERENCES T_USER (id) ON DELETE RESTRICT
);

CREATE TABLE T_CONTACT
(
    contactor_id VARCHAR(45) NOT NULL,
    contacted_id VARCHAR(45) NOT NULL,
    PRIMARY KEY (contactor_id, contacted_id),
    FOREIGN KEY (contactor_id) REFERENCES T_USER (id),
    FOREIGN KEY (contacted_id) REFERENCES T_USER (id)

);

CREATE TABLE T_BANK_TRANSFERT
(
    id                LONG,
    transaction_owner VARCHAR(100) NOT NULL,
    bank_id           INTEGER      NOT NULL,
    transfert_type    VARCHAR(255) NOT NULL,
    amount            FLOAT        NOT NULL,
    date              TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (transaction_owner) REFERENCES T_USER (id) ON DELETE RESTRICT,
    FOREIGN KEY (bank_id) REFERENCES T_BANKACCOUNT (ba_id) ON DELETE RESTRICT
);