INSERT INTO T_USER(id, password, balance,authority)
VALUES ('thomas@gmail.com', '$2a$12$VNrcLt828CpF1vZx/dWXc.KzAiOrBl9M6d4a3n8JJLe1BtkXmHHDS', 20, 'ROLE_ADMIN'),
       ('kevin@gmail.com', '$2a$12$VNrcLt828CpF1vZx/dWXc.KzAiOrBl9M6d4a3n8JJLe1BtkXmHHDS', 52, 'ROLE_USER'),
       ('romain@gmail.com', '$2a$12$VNrcLt828CpF1vZx/dWXc.KzAiOrBl9M6d4a3n8JJLe1BtkXmHHDS', 5246, 'ROLE_USER'),
       ('bertha@gmail.com', '$2a$12$VNrcLt828CpF1vZx/dWXc.KzAiOrBl9M6d4a3n8JJLe1BtkXmHHDS', 6524, 'ROLE_USER'),
       ('rodriguez@gmail.com', '$2a$12$VNrcLt828CpF1vZx/dWXc.KzAiOrBl9M6d4a3n8JJLe1BtkXmHHDS', 86, 'ROLE_USER');

INSERT INTO T_BANKACCOUNT(ba_id, OWNER_ID, bic, iban)
VALUES (1, 'thomas@gmail.com', '12341235415', '1542451248754542145715248756254875'),
       (2, 'kevin@gmail.com', '35428467852', '1542451248754542145715248756254875'),
       (3, 'romain@gmail.com', '35428467852', '1542451248754542145715248756254873'),
       (4, 'bertha@gmail.com', '35428467852', '1542451248754542145715248756254875'),
       (5, 'rodriguez@gmail.com', '35428467852', '1542451248754542145715248756254853');

INSERT INTO T_PAYMENT(p_id, creditor_id, debtor_id, amount, fees, date, message)
VALUES (0, 'thomas@gmail.com', 'bertha@gmail.com', 12032.012, 601.6006, CURRENT_TIMESTAMP, 'food'),
       (1, 'romain@gmail.com', 'thomas@gmail.com', 12032.012, 601.6006, CURRENT_TIMESTAMP, 'Burger'),
       (2, 'bertha@gmail.com', 'kevin@gmail.com', 12032.012, 601.6006, CURRENT_TIMESTAMP, 'Restaurant'),
       (3, 'romain@gmail.com', 'thomas@gmail.com', 12032.012, 601.6006, CURRENT_TIMESTAMP, 'Movie'),
       (4, 'thomas@gmail.com', 'kevin@gmail.com', 12032.012, 601.6006, CURRENT_TIMESTAMP, 'Bar');

INSERT INTO T_CONTACT(contactor_id, contacted_id)
VALUES ('thomas@gmail.com', 'bertha@gmail.com'),
       ('thomas@gmail.com', 'romain@gmail.com'),
       ('thomas@gmail.com', 'kevin@gmail.com'),
       ('romain@gmail.com', 'kevin@gmail.com'),
       ('bertha@gmail.com', 'kevin@gmail.com');


INSERT INTO T_BANK_TRANSFERT(id, transaction_owner, transfert_type, amount, date, bank_id)
VALUES (1, 'thomas@gmail.com', 'DEBITBANK', 12032.012, CURRENT_TIMESTAMP, 1),
       (2, 'thomas@gmail.com', 'CREDITBANK', 32.012, CURRENT_TIMESTAMP, 1),
       (3, 'bertha@gmail.com', 'DEBITBANK', 12032.012, CURRENT_TIMESTAMP, 4),
       (4, 'bertha@gmail.com', 'CREDITBANK', 132.012, CURRENT_TIMESTAMP, 4),
       (5, 'romain@gmail.com', 'DEBITBANK', 12032.012, CURRENT_TIMESTAMP, 3);