package com.paymybuddy.app.services;

import com.paymybuddy.app.constants.EActionsConstants;
import com.paymybuddy.app.database.models.BankAccount;
import com.paymybuddy.app.database.models.BankTransfert;
import com.paymybuddy.app.database.models.Contact;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.database.models.constants.TransfertType;
import com.paymybuddy.app.database.repositories.BankAccountRepository;
import com.paymybuddy.app.database.repositories.BankTransfertRepository;
import com.paymybuddy.app.database.repositories.UserRepository;
import com.paymybuddy.app.helper.Factory.BankAccountFactory;
import com.paymybuddy.app.helper.Factory.BankTransfertFactory;
import com.paymybuddy.app.helper.Factory.ContactFactory;
import com.paymybuddy.app.helper.Factory.ContactPrimaryKeyFactory;
import com.paymybuddy.app.helper.Factory.UserFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class BankTransfertServicesTest {

    @Autowired
    BankTransfertServices bankTransfertServices;

    @MockBean
    UserRepository userRepository;

    @MockBean
    BankAccountRepository bankAccountRepository;

    @MockBean
    BankTransfertRepository bankTransfertRepository;

    String idRocco = "rocco@gmail.com";
    String passRocco = "blabla25";
    String idSam = "sam@gmail.com";
    String passSam = "samsamsam";
    String iban = "1234567891123456789123456789123454";
    String bic = "12345467891";

    User rocco;

    User sam;

    Contact contactSamRocco;

    BankAccount ba;

    Float amount;

    BankTransfert bt;

    @BeforeEach
    void prepare() {
        rocco = UserFactory.createUser(idRocco, passRocco);
        sam = UserFactory.createUser(idSam, passSam);
        contactSamRocco = ContactFactory.createContact(ContactPrimaryKeyFactory.createCpk(sam, rocco));
        ba = BankAccountFactory.createBankAccount(1L,rocco, iban, bic);
        rocco = UserFactory.createUser(idRocco,passRocco,ba, 70F);
        bt = BankTransfertFactory.createBankTransfert(1L,TransfertType.DEBITBANK, 50F, rocco, ba);

    }

    @Test
    void transfertMoneyFromBankAccountToBalanceTest_ok() {
        //GIVEN
        amount = 50F;
        when(bankTransfertRepository.save(any())).thenReturn(bt);
        when(bankTransfertRepository.findById(0L)).thenReturn(Optional.ofNullable(bt));
        when(bankAccountRepository.save(ba)).thenReturn(ba);
        when(userRepository.save(ArgumentMatchers.any())).thenReturn(rocco);
        //WHEN
        BankTransfert bankTransfertTest = bankTransfertServices.transfertMoneyFromBankToBalance(amount, rocco, ba);
        Optional<BankTransfert> bankTransfertbdd = bankTransfertRepository.findById(0L);
        //THEN
        assertThat(bankTransfertTest).isEqualTo(bt);
        assertThat(bankTransfertbdd).contains(bt);
        assertThat(bankTransfertTest.getDate()).isEqualTo(bt.getDate());
        assertThat(bankTransfertTest.getTransfertType()).isEqualTo(bt.getTransfertType());
        assertThat(bankTransfertTest.getBankAccount()).isEqualTo(ba);
        assertThat(bankTransfertTest.getOwnerId()).isEqualTo(rocco);
        assertThat(bankTransfertTest.getId()).isEqualTo(bt.getId());

    }


    @Test
    void transfertMoneyFromBankAccountToBalanceTest_amountNegative() {
        //GIVEN
        amount = -10F;
        when(bankTransfertRepository.findById(0L)).thenReturn(Optional.empty());
        //WHEN
        assertThatThrownBy(() -> bankTransfertServices.transfertMoneyFromBankToBalance(amount, rocco, ba))
                .isInstanceOf(RuntimeException.class)
                .hasMessage(EActionsConstants.TRANSACTION_ERROR_NEGATIVE_AMOUNT.getValue());

    }

    @Test
    void transfertMoneyFromBalanceToBankAccountTest_ok() {
        //GIVEN
        amount = 50F;
        when(bankTransfertRepository.save(any())).thenReturn(bt);
        when(bankTransfertRepository.findById(0L)).thenReturn(Optional.ofNullable(bt));
        when(bankAccountRepository.save(ba)).thenReturn(ba);
        when(userRepository.save(ArgumentMatchers.any())).thenReturn(rocco);
        //WHEN
        BankTransfert bankTransfertTest = bankTransfertServices.transfertMoneyFromBalanceToBankAccount(amount, rocco, ba);
        Optional<BankTransfert> bankTransfertbdd = bankTransfertRepository.findById(0L);
        //THEN
        assertThat(bankTransfertTest).isEqualTo(bt);
        assertThat(bankTransfertbdd).contains(bt);
        assertThat(bankTransfertTest.getDate()).isEqualTo(bt.getDate());
        assertThat(bankTransfertTest.getTransfertType()).isEqualTo(bt.getTransfertType());
        assertThat(bankTransfertTest.getBankAccount()).isEqualTo(ba);
        assertThat(bankTransfertTest.getOwnerId()).isEqualTo(rocco);
        assertThat(bankTransfertTest.getId()).isEqualTo(bt.getId());
    }

    @Test
    void transfertMoneyFromBalanceToBankAccountTest_amountTooHigh() {

        //GIVEN
        amount = 80F;
        when(bankTransfertRepository.findById(0L)).thenReturn(Optional.empty());
        //WHEN
        assertThatThrownBy(() -> bankTransfertServices.transfertMoneyFromBalanceToBankAccount(amount, rocco, ba))
                .isInstanceOf(RuntimeException.class)
                .hasMessage(EActionsConstants.TRANSACTION_BALANCE_ERROR_AMOUNT_TOO_HIGH.getValue());
    }

    @Test
    void transfertMoneyFromBalanceToBankAccountTest_amountNegative() {
        //GIVEN
        amount = -10F;
        //WHEN
        assertThatThrownBy(() -> bankTransfertServices.transfertMoneyFromBalanceToBankAccount(amount, rocco, ba))
                .isInstanceOf(RuntimeException.class)
                .hasMessage(EActionsConstants.TRANSACTION_ERROR_NEGATIVE_AMOUNT.getValue());
    }
}
