package com.paymybuddy.app.services;

import com.paymybuddy.app.database.models.Contact;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.database.repositories.UserRepository;
import com.paymybuddy.app.helper.Factory.ContactFactory;
import com.paymybuddy.app.helper.Factory.ContactPrimaryKeyFactory;
import com.paymybuddy.app.helper.Factory.UserFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class UserServicesTest {

    @Autowired
    UserServices userServicesUnderTest;

    @MockBean
    UserRepository userRepository;

    String idRocco = "rocco@gmail.com";
    String passRocco = "blabla25";
    String idSam = "sam@gmail.com";
    String passSam = "samsamsam";

    User rocco;

    User sam;

    Contact contactSamRocco;

    @BeforeEach
    void prepare() {
        rocco = UserFactory.createUser(idRocco, passRocco);
        sam = UserFactory.createUser(idSam, passSam);
        contactSamRocco = ContactFactory.createContact(ContactPrimaryKeyFactory.createCpk(sam, rocco));
    }

    @Test
    void createUserTest_succeed() {
        //GIVEN
        when(userRepository.save(ArgumentMatchers.any())).thenReturn(rocco);
        //WHEN
        User usertest = userServicesUnderTest.createUser(idRocco, passRocco);
        //THEN
        assertThat(usertest).isEqualTo(rocco);
    }

    @Test
    void createUserTest_userAlreadyCreated() {
        //GIVEN
        String idThomas = "thomas@gmail.com";
        when(userRepository.findById(idThomas)).thenReturn(Optional.of(rocco));
        //WHEN
        User usertest = userServicesUnderTest.createUser(idThomas, passRocco);
        //THEN
        assertThat(usertest).isNull();
    }

    @Test
    void connectUserTest_succeed() {
        //GIVEN
        userRepository.save(rocco);
        when(userRepository.findById(idRocco)).thenReturn(Optional.ofNullable(rocco));
        //WHEN
        boolean test = userServicesUnderTest.connectWithUser(idRocco, passRocco);
        assertThat(test).isTrue();
    }

    @Test
    void connectUserTest_wrongPassword() {
        //GIVEN
        String wrongPassword = "cocococo";
        userRepository.save(rocco);
        when(userRepository.findById(idRocco)).thenReturn(Optional.ofNullable(rocco));
        //WHEN
        boolean test = userServicesUnderTest.connectWithUser(idRocco, wrongPassword);
        assertThat(test).isFalse();
    }

    @Test
    void connectUserTest_unknownUser() {
        //GIVEN
        String unknown = "unknown@gmail.com";
        String wrongPassword = "cocococo";
        userRepository.save(rocco);
        when(userRepository.findById(unknown)).thenReturn(Optional.empty());
        //WHEN
        boolean test = userServicesUnderTest.connectWithUser(unknown, wrongPassword);
        assertThat(test).isFalse();
    }

    @Test
    void getUser_succeed() {
        //GIVEN
        userRepository.save(rocco);
        when(userRepository.findById(idRocco)).thenReturn(Optional.of(rocco));
        //WHEN
        User test = userServicesUnderTest.getUser(idRocco);
        //THEN
        assertThat(test).isEqualTo(rocco);
    }
}
