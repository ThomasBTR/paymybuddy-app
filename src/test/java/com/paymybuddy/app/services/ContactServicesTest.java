package com.paymybuddy.app.services;

import com.paymybuddy.app.database.models.Contact;
import com.paymybuddy.app.database.models.ContactPrimaryKey;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.database.repositories.ContactRepository;
import com.paymybuddy.app.database.repositories.UserRepository;
import com.paymybuddy.app.helper.Factory.ContactFactory;
import com.paymybuddy.app.helper.Factory.ContactPrimaryKeyFactory;
import com.paymybuddy.app.helper.Factory.UserFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class ContactServicesTest {

    @Autowired
    ContactServices contactServices;

    @MockBean
    UserRepository userRepository;

    @MockBean
    ContactRepository contactRepository;

    String idRocco = "rocco@gmail.com";
    String passRocco = "blabla25";
    String idSam = "sam@gmail.com";
    String passSam = "samsamsam";

    User rocco;

    User sam;

    Contact contactSamRocco;

    ContactPrimaryKey primaryKey;

    @BeforeEach
    void prepare() {
        primaryKey = ContactPrimaryKeyFactory.createCpk(sam, rocco);
        rocco = UserFactory.createUser(idRocco, passRocco);
        sam = UserFactory.createUser(idSam, passSam);
        contactSamRocco = ContactFactory.createContact(primaryKey);

    }

    @Test
    void addContactTest() {
        //GIVEN
        userRepository.save(rocco);
        userRepository.save(sam);
        when(userRepository.findById(idRocco)).thenReturn(Optional.ofNullable(rocco));
        when(userRepository.findById(idSam)).thenReturn(Optional.ofNullable(sam));
        when(userRepository.save(any())).thenReturn(rocco);
        when(contactRepository.save(any())).thenReturn(contactSamRocco);
        //WHEN
        User roccoBdd = contactServices.addContact(idRocco, idSam);
        //THEN
        assertThat(roccoBdd.getContacts()).hasSize(1);
        assertThat(roccoBdd.getContacts()).contains(sam);
        assertThat(roccoBdd.getContacts().get(0)).isEqualTo(sam);
    }

    @Test
    void getContact_Success(){
        //GIVEN
        when(userRepository.findById(idRocco)).thenReturn(Optional.ofNullable(rocco));
        when(userRepository.findById(idSam)).thenReturn(Optional.ofNullable(sam));
        when(userRepository.save(any())).thenReturn(rocco);
        when(contactRepository.save(any())).thenReturn(contactSamRocco);
        contactServices.addContact(idRocco, idSam);
        //WHEN
        List<User> test = contactServices.getContacts(idRocco);
        //THEN
        assertThat(test).hasSize(1)
                .contains(sam);
    }
}
