package com.paymybuddy.app.services;

import com.paymybuddy.app.database.models.BankAccount;
import com.paymybuddy.app.database.models.Contact;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.database.repositories.BankAccountRepository;
import com.paymybuddy.app.database.repositories.UserRepository;
import com.paymybuddy.app.helper.Factory.ContactFactory;
import com.paymybuddy.app.helper.Factory.ContactPrimaryKeyFactory;
import com.paymybuddy.app.helper.Factory.UserFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class BankAccountServicesTest {

    @Autowired
    BankAccountServices bankAccountServices;

    @MockBean
    UserRepository userRepository;

    @MockBean
    BankAccountRepository bankAccountRepository;

    String idRocco = "rocco@gmail.com";
    String passRocco = "blabla25";
    String idSam = "sam@gmail.com";
    String passSam = "samsamsam";
    String iban = "1234567891123456789123456789123454";
    String bic = "12345467891";

    User rocco;

    User sam;

    Contact contactSamRocco;

    BankAccount ba;

    @BeforeEach
    void prepare() {
        rocco = UserFactory.createUser(idRocco, passRocco);
        sam = UserFactory.createUser(idSam, passSam);
        contactSamRocco = ContactFactory.createContact(ContactPrimaryKeyFactory.createCpk(sam, rocco));
        ba = new BankAccount(0L, rocco, iban, bic);

    }

    @Test
    void addBankAccountTest() {
        //GIVEN
        when(userRepository.findById(any())).thenReturn(Optional.ofNullable(rocco));
        when(userRepository.save(ArgumentMatchers.any())).thenReturn(rocco);
        //WHEN
        User usertest = bankAccountServices.addBankAccount(idRocco, iban, bic);
        //THEN
        assertThat(usertest).isEqualTo(rocco);
        assertThat(usertest.getBankAccounts()).hasSize(1);
        assertThat(usertest.getBankAccounts()).contains(ba);
        assertThat(usertest.getBankAccounts().get(0).getIban()).isEqualTo(iban);
        assertThat(usertest.getBankAccounts().get(0).getBic()).isEqualTo(bic);

    }

    @Test
    void getBankAccountTest(){
        //GIVEN

        List<BankAccount> bankAccountList = new ArrayList<>();
        bankAccountList.add(ba);
        when(bankAccountRepository.getBankAccountsByIban(iban)).thenReturn(bankAccountList);
        when(userRepository.getById(idRocco)).thenReturn(rocco);
        //WHEN
        BankAccount baUnderTest = bankAccountServices.getBankAccount(idRocco, iban);

        //THEN
        assertThat(baUnderTest.getIban()).isEqualTo(iban);
        assertThat(baUnderTest.getBic()).isEqualTo(bic);
        assertThat(baUnderTest.getOwner().getId()).isEqualTo(idRocco);
    }
}
