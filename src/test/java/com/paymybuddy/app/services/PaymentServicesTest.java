package com.paymybuddy.app.services;

import com.paymybuddy.app.constants.EActionsConstants;
import com.paymybuddy.app.database.models.BankAccount;
import com.paymybuddy.app.database.models.Contact;
import com.paymybuddy.app.database.models.Payment;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.database.repositories.PaymentRepository;
import com.paymybuddy.app.database.repositories.UserRepository;
import com.paymybuddy.app.helper.Factory.BankAccountFactory;
import com.paymybuddy.app.helper.Factory.ContactFactory;
import com.paymybuddy.app.helper.Factory.ContactPrimaryKeyFactory;
import com.paymybuddy.app.helper.Factory.PaymentFactory;
import com.paymybuddy.app.helper.Factory.UserFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class PaymentServicesTest {

    @Autowired
    PaymentServices paymentServices;

    @MockBean
    UserRepository userRepository;

    @MockBean
    PaymentRepository paymentRepository;

    String idRocco = "rocco@gmail.com";
    String passRocco = "blabla25";
    String idSam = "sam@gmail.com";
    String passSam = "samsamsam";
    String iban = "1234567891123456789123456789123454";
    String bic = "12345467891";

    User rocco;

    User sam;

    Contact contactSamRocco;

    BankAccount ba;

    Float amount;

    Payment payment;

    @BeforeEach
    void prepare() {
        rocco = UserFactory.createUser(idRocco, passRocco);
        sam = UserFactory.createUser(idSam, passSam);
        contactSamRocco = ContactFactory.createContact(ContactPrimaryKeyFactory.createCpk(sam, rocco));
        ba = BankAccountFactory.createBankAccount(1L,rocco, iban, bic);
        rocco = UserFactory.createUser(idRocco,passRocco,ba, 70F);
        payment = PaymentFactory.createPayment(rocco,sam, 1L,50F);

    }

    @Test
    void transfertMoneyTest_ok() {
        //GIVEN
        amount = 50F;
        when(paymentRepository.save(any())).thenReturn(payment);
        when(paymentRepository.findById(1L)).thenReturn(Optional.ofNullable(payment));
        when(userRepository.save(rocco)).thenReturn(rocco);
        when(userRepository.save(sam)).thenReturn(sam);
        when(paymentRepository.save(ArgumentMatchers.any())).thenReturn(payment);
        //WHEN
        Payment paymentUnderTest = paymentServices.transfertMoney(amount, sam, rocco);
        Optional<Payment> paymentRepositoryById = paymentRepository.findById(1L);
        //THEN
        assertThat(paymentUnderTest).isEqualTo(payment);
        assertThat(paymentRepositoryById).contains(payment);
        assertThat(paymentUnderTest.getDate()).isEqualTo(payment.getDate());
        assertThat(paymentUnderTest.getFees()).isEqualTo(payment.getFees());
        assertThat(paymentUnderTest.getAmount()).isEqualTo(payment.getAmount());
        assertThat(paymentUnderTest.getId()).isEqualTo(paymentUnderTest.getId());
        assertThat(paymentUnderTest.getDate()).isEqualTo(payment.getDate());
        assertThat(paymentUnderTest.getDebtor()).isEqualTo(payment.getDebtor());
        assertThat(paymentUnderTest.getCreditor()).isEqualTo(payment.getCreditor());
    }

    @Test
    void transfertMoneyTest_amountNegative() {
        //GIVEN
        amount = -10F;
        when(paymentRepository.findById(1L)).thenReturn(Optional.empty());
        //WHEN
        assertThatThrownBy(() -> paymentServices.transfertMoney(amount, rocco, sam))
                .isInstanceOf(RuntimeException.class)
                .hasMessage(EActionsConstants.TRANSACTION_ERROR_NEGATIVE_AMOUNT.getValue());
    }

    @Test
    void transfertMoneyTest_amountTooHigh() {
        //GIVEN
        amount = 80F;
        //WHEN
        assertThatThrownBy(() -> paymentServices.transfertMoney(amount, rocco, sam))
                .isInstanceOf(RuntimeException.class)
                .hasMessage(EActionsConstants.TRANSACTION_BALANCE_ERROR_AMOUNT_TOO_HIGH.getValue());
    }

}
