package com.paymybuddy.app.database;

import com.paymybuddy.app.database.models.BankAccount;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.database.repositories.BankAccountRepository;
import com.paymybuddy.app.database.repositories.UserRepository;
import com.paymybuddy.app.helper.Factory.BankAccountFactory;
import com.paymybuddy.app.helper.Factory.UserFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
class BankAccountRepositoryTest {

    @Autowired
    BankAccountRepository bankAccountRepository;

    @Autowired
    UserRepository userRepository;

    User thomas;

    BankAccount ba;

    @BeforeEach
    void prepare(){
        thomas = UserFactory.createUser("toto@gmail.com", "azerty");
        userRepository.save(this.thomas);

        ba = BankAccountFactory.createBankAccount(1L,thomas,"12345678912","1234567894561234567897453489");

    }

    @Test
    void testFindAll() {
        List<BankAccount> users = bankAccountRepository.findAll();
        assertThat(users).hasSize(5);
    }
    @Test
    void test_bankAccount_retreive_verify() {
        //WHEN
        BankAccount baSavedInDb = bankAccountRepository.save(this.ba);
        Optional<BankAccount> baFetchFromDb = bankAccountRepository.findById(1L);
        //THEN
        assertThat(baSavedInDb).isEqualTo(ba);
        assertThat(baFetchFromDb).contains(ba);
        assertThat(baSavedInDb.getId()).isEqualTo(ba.getId());
        assertThat(baSavedInDb.getBic()).isEqualTo(ba.getBic());
        assertThat(baSavedInDb.getIban()).isEqualTo(ba.getIban());
        assertThat(baSavedInDb.hashCode()).isNotZero();
        assertThat(baSavedInDb.toString()).hasToString(ba.toString());
    }

}
