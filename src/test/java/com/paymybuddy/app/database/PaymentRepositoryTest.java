package com.paymybuddy.app.database;

import com.paymybuddy.app.database.models.Payment;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.database.repositories.PaymentRepository;
import com.paymybuddy.app.database.repositories.UserRepository;
import com.paymybuddy.app.helper.Factory.PaymentFactory;
import com.paymybuddy.app.helper.Factory.UserFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
class PaymentRepositoryTest {

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    UserRepository userRepository;

    User thomas;

    User gary;

    Payment payment1;

    Payment payment2;

    @BeforeEach
    void prepare(){
        thomas = UserFactory.createUser("toto@gmail.com", "azerty");
        gary = UserFactory.createUser("gary@gmail.com", "berthooo");

        userRepository.save(this.thomas);
        userRepository.save(this.gary);

        payment1 = PaymentFactory.createPayment(thomas,gary,1L);
        payment2 = PaymentFactory.createPayment(thomas,gary,2L,1100F);
    }

    @Test
    void testFindAll() {
        List<Payment> users = paymentRepository.findAll();
        assertThat(users).hasSize(5);
    }

    @Test
    void testPaymentSaveAndRetrive(){
        //GIVEN
        //WHEN
        Payment pSavedInDb = paymentRepository.save(payment1);
        Optional<Payment> pFetchFromDb = paymentRepository.findById(1L);
        //THEN
        assertThat(pSavedInDb).isEqualTo(payment1);
        assertThat(pFetchFromDb).contains(payment1);
        assertThat(pSavedInDb.getFees()).isEqualTo(payment1.getFees());
        assertThat(pSavedInDb.getDate()).isEqualTo(payment1.getDate());
        assertThat(pSavedInDb.getAmount()).isEqualTo(payment1.getAmount());
        assertThat(pSavedInDb.getCreditor()).isEqualTo(payment1.getCreditor());
        assertThat(pSavedInDb.getDebtor()).isEqualTo(payment1.getDebtor());
        assertThat(pSavedInDb.toString()).hasToString(payment1.toString());
        assertThat(pSavedInDb.hashCode()).isNotZero();
    }

}
