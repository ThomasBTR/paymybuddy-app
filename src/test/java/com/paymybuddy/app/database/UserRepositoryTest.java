package com.paymybuddy.app.database;

import com.paymybuddy.app.database.models.BankAccount;
import com.paymybuddy.app.database.models.BankTransfert;
import com.paymybuddy.app.database.models.Payment;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.database.models.constants.TransfertType;
import com.paymybuddy.app.database.repositories.BankAccountRepository;
import com.paymybuddy.app.database.repositories.BankTransfertRepository;
import com.paymybuddy.app.database.repositories.PaymentRepository;
import com.paymybuddy.app.database.repositories.UserRepository;
import com.paymybuddy.app.helper.Factory.BankAccountFactory;
import com.paymybuddy.app.helper.Factory.BankTransfertFactory;
import com.paymybuddy.app.helper.Factory.PaymentFactory;
import com.paymybuddy.app.helper.Factory.UserFactory;
import com.paymybuddy.app.helper.UTHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    BankTransfertRepository bankTransfertRepository;

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    BankAccountRepository bankAccountRepository;

    User thomas = new User();

    User gary = new User();

    User sam = new User();

    BankAccount ba =  new BankAccount();

    Payment pGarySam = new Payment();

    Payment pSamGary = new Payment();
    BankTransfert bankTransfert = new BankTransfert();

    @BeforeEach
    public void setUp() {
        thomas = UserFactory.createUser("toto@gmail.com", "azerty", null, null, null, null, null,null);
        gary = UserFactory.createUser("gary@gmail.com", "berthooo", null, null, null, null, null,null);
        sam = UserFactory.createUser("sam@gmail.com", "aserty");
        ba = BankAccountFactory.createBankAccount(sam);
        userRepository.save(sam);
        userRepository.save(this.gary);
        pGarySam = PaymentFactory.createPayment(gary, sam, 1L);
        pSamGary = PaymentFactory.createPayment(sam, gary, 2L);
        bankTransfert =  BankTransfertFactory.createBankTransfert(1L,TransfertType.DEBITBANK,200F,sam, ba);
        sam = UserFactory.createUser("sam@gmail.com", "aserty", ba, gary, gary,pGarySam , pSamGary, bankTransfert );
    }

    @Test
    void testFindAllUsers() {
        List<User> users = userRepository.findAllUser();
        assertThat(users).hasSize(7);
    }

    @Test
    void testsaveThomas() {
        //GIVEN
        //WHEN
        User totoSavedInDB = userRepository.save(this.thomas);
        Optional<User> totoFindInDb = userRepository.findById("toto@gmail.com");
        //THEN
        assertThat(totoSavedInDB).isEqualTo(thomas);
        assertThat(totoFindInDb).contains(thomas);
    }

    @Test
    void test_save_retreive_verify_sam() {
        //GIVEN
        ba = bankAccountRepository.save(ba);
        pSamGary =paymentRepository.save(pSamGary);
        pGarySam = paymentRepository.save(pGarySam);
        bankTransfert =  bankTransfertRepository.save(bankTransfert);
        //WHEN
        User samSavedInDb = userRepository.save(this.sam);
        Optional<User> samFetchFromDb = userRepository.findById("sam@gmail.com");
        //THEN
        assertThat(samSavedInDb).isEqualTo(sam);
        assertThat(samFetchFromDb).contains(sam);
        assertThat(samSavedInDb.getBalance()).isEqualTo(sam.getBalance());
        assertThat(samSavedInDb.getPassword()).isEqualTo(sam.getPassword());
        assertThat(samSavedInDb.hashCode()).isNotZero();
        assertThat(samSavedInDb.toString()).hasToString(sam.toString());
    }


    @Test
    void testFindOneUser() throws IOException {
        //GIVE
        User expected = UTHelper.stringToObject(UTHelper.readFileAsString("expected/user/bertha.json"), User.class);
        //WHEN
        Optional<User> actual = userRepository.findById("bertha@gmail.com");
        //THEN
        assertThat(actual).contains(expected);
    }

}
