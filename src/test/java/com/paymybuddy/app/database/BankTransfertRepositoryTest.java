package com.paymybuddy.app.database;

import com.paymybuddy.app.database.models.BankAccount;
import com.paymybuddy.app.database.models.BankTransfert;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.database.models.constants.TransfertType;
import com.paymybuddy.app.database.repositories.BankAccountRepository;
import com.paymybuddy.app.database.repositories.BankTransfertRepository;
import com.paymybuddy.app.database.repositories.UserRepository;
import com.paymybuddy.app.helper.Factory.BankAccountFactory;
import com.paymybuddy.app.helper.Factory.BankTransfertFactory;
import com.paymybuddy.app.helper.Factory.UserFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
class BankTransfertRepositoryTest {

    @Autowired
    BankTransfertRepository bankTransfertRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BankAccountRepository bankAccountRepository;

    User thomas;

    BankTransfert bt;

    BankAccount ba;

    @BeforeEach
    void prepare(){

        thomas = UserFactory.createUser("toto@gmail.com", "azerty", ba);
        ba = BankAccountFactory.createBankAccount(thomas);
        userRepository.save(this.thomas);
        bankAccountRepository.save(ba);

        bt = BankTransfertFactory.createBankTransfert(1L, TransfertType.DEBITBANK,100F,thomas, ba);

    }

    @Test
    void testFindAll() {
        List<BankTransfert> bankTransferts = bankTransfertRepository.findAll();
        assertThat(bankTransferts).hasSize(5);
    }

    @Test
    void test_bankAccount_retreive_verify() {
        //WHEN
        BankTransfert baSavedInDb = bankTransfertRepository.save(this.bt);
        Optional<BankTransfert> baFetchFromDb = bankTransfertRepository.findById(1L);
        //THEN
        assertThat(baSavedInDb).isEqualTo(bt);
        assertThat(baFetchFromDb).contains(bt);
        assertThat(baSavedInDb.getId()).isEqualTo(bt.getId());
        assertThat(baSavedInDb.getAmount()).isEqualTo(bt.getAmount());
        assertThat(baSavedInDb.getId()).isEqualTo(bt.getId());
        assertThat(baSavedInDb.hashCode()).isNotZero();
        assertThat(baSavedInDb.toString()).hasToString(bt.toString());
        assertThat(baSavedInDb.getTransfertType()).isEqualTo(bt.getTransfertType());
        assertThat(baSavedInDb.getDate()).isEqualTo(bt.getDate());
        assertThat(baSavedInDb.getOwnerId()).isEqualTo(bt.getOwnerId());
    }

}
