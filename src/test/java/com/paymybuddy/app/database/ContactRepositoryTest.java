package com.paymybuddy.app.database;

import com.paymybuddy.app.database.models.Contact;
import com.paymybuddy.app.database.models.ContactPrimaryKey;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.database.repositories.ContactRepository;
import com.paymybuddy.app.database.repositories.UserRepository;
import com.paymybuddy.app.helper.Factory.ContactFactory;
import com.paymybuddy.app.helper.Factory.ContactPrimaryKeyFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
class ContactRepositoryTest {

    @Autowired
    ContactRepository contactRepository;

    @Autowired
    UserRepository userRepository;

    User thomas;

    User bertha;

    ContactPrimaryKey contactPrimaryKey;

    Contact contact;


    @BeforeEach
    void prepare() {
        thomas = userRepository.getById("thomas@gmail.com");
        bertha = userRepository.getById("bertha@gmail.com");

        contactPrimaryKey = ContactPrimaryKeyFactory.createCpk(thomas, bertha);

        contact = ContactFactory.createContact(contactPrimaryKey);
    }

    @Test
    void testFindAll() {
        List<Contact> users = contactRepository.findAll();
        assertThat(users).hasSize(5);
    }

    @Test
    void testOneContact() {
        //GIVEN
        //WHEN
        List<Contact> contacts = contactRepository.findByIdContactedId(bertha);
        // THEN
        assertThat(contacts).hasSize(1);
        assertThat(contacts.get(0)).isInstanceOf(Contact.class);
    }

    @Test
    void test_contact_save_verify() {
        //GIVEN
        //WHEN
        Contact cSavedInDb = contactRepository.save(this.contact);
        List<Contact> cFetchFromDb = contactRepository.findByIdContactorId(thomas);
        //THEN
        assertThat(cSavedInDb.hashCode()).hasSameHashCodeAs(this.contact.hashCode());
        assertThat(cFetchFromDb.get(0).hashCode()).hasSameHashCodeAs(this.contact.hashCode());
        assertThat(cSavedInDb.toString()).hasToString(this.contact.toString());
        assertThat(cSavedInDb.getId().getContactedId().toString()).hasToString(bertha.toString());
        assertThat(cSavedInDb.getId().getContactorId().toString()).hasToString(thomas.toString());
        assertThat(cSavedInDb.getId()).isEqualTo(contactPrimaryKey);
        assertThat(cSavedInDb).isEqualTo(contact);
    }
}
