package com.paymybuddy.app.controllers;

import com.paymybuddy.app.controllers.utils.CustomOidcUserService;
import com.paymybuddy.app.controllers.utils.OidcUserInfo;
import com.paymybuddy.app.controllers.utils.UsernameGetter;
import com.paymybuddy.app.database.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.test.annotation.DirtiesContext;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class UtilsTests {

    @Autowired
    private UsernameGetter usernameGetter;

    @Autowired
    private OidcUserInfo oidcUserInfo;

    @MockBean
    private UserRepository userRepository;

    private CustomOidcUserService customOidcUserService;

    private Authentication authenticationOAuth2;

    private Authentication authenticationOidc;

    private Map<String, Object> attributes;

    private DefaultOidcUser oidcUser;

    private Collection<OAuth2UserAuthority> authorities;

    private OidcIdToken oidcIdToken;


    @BeforeEach
    void prepare() {
        customOidcUserService = new CustomOidcUserService(userRepository);

        Map<String, Object> oAuth2UserAuthorityMap = new HashMap<>();
        oAuth2UserAuthorityMap.put("ROLE", "ROLE_USER");
        OAuth2UserAuthority auth2UserAuthority = new OAuth2UserAuthority(oAuth2UserAuthorityMap);
        authorities = new ArrayList<>();
        authorities.add(auth2UserAuthority);
        attributes = new HashMap<>();
        attributes.put("email", "thomas@gmail.com");
        attributes.put("id", "azerty");
        DefaultOAuth2User oAuth2User = new DefaultOAuth2User(authorities, attributes, "email");
        authenticationOAuth2 = new OAuth2AuthenticationToken(oAuth2User, authorities, "abcdef");

        attributes.put("sub", "12345");
        oidcIdToken = new OidcIdToken("123456", Instant.now(), Instant.MAX, attributes);

        oidcUser = new DefaultOidcUser(authorities, oidcIdToken);
        authenticationOidc = new OAuth2AuthenticationToken(oidcUser, authorities, "abcdef");
    }


    @Test
    void testUsernameGetterOAuth2_ok() {
        String expectedUsername = "thomas@gmail.com";
        //WHEN
        String actualUsername = usernameGetter.usernameGetter(authenticationOAuth2);
        // THEN
        assertThat(actualUsername).isEqualTo(expectedUsername);

    }

    @Test
    void testUsernameGetterOidc_ok() {
        String expectedUsername = "thomas@gmail.com";
        //WHEN
        String actualUsername = usernameGetter.usernameGetter(authenticationOidc);
        // THEN
        assertThat(actualUsername).isEqualTo(expectedUsername);

    }


    @Test
    void testCustomOidcUserService() {
        //GIVEN
        oidcUser = new DefaultOidcUser(authorities, oidcIdToken);
        //WHEN
        OidcUser actualOidcUser = customOidcUserService.processOidcUser(oidcUser);
        //THEN
        assertThat(actualOidcUser).isEqualTo(oidcUser);
    }

}
