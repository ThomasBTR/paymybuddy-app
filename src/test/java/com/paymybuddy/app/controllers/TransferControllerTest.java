package com.paymybuddy.app.controllers;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.annotation.DirtiesContext.ClassMode;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
class TransferControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Order(1)
    @WithMockUser(username = "thomas@gmail.com", password = "azerty", authorities = "ROLE_USER")
    void testGetIndex_OK() throws Exception {
        mockMvc.perform(get("/user/transfer"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("contacts", hasSize(3)))
                .andExpect(model().attribute("payments", hasSize(2)));
    }

    @Test
    @Order(2)
    @WithMockUser(username = "thomas@gmail.com", password = "azerty", authorities = "ROLE_USER")
    void testPostContact_OK() throws Exception {
        String amount = "5";
        String contactId = "kevin@gmail.com";
        mockMvc.perform(post("/user/transfer").param("amount", amount).param("contactId", contactId).with(csrf()))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @Order(3)
    @WithMockUser(username = "thomas@gmail.com", password = "azerty", authorities = "ROLE_USER")
    void testPostProfile_OK() throws Exception {
        String amount = "10";
        String iban = "1542451248754542145715248756254875";
        String transferType = "Bank Account Credit";
        mockMvc.perform(post("/user/profile").param("amount", amount).param("iban", iban).param("transferType", transferType).with(csrf()))
                .andExpect(status().is2xxSuccessful());
    }

}
