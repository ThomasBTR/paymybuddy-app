package com.paymybuddy.app.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testGetIndex_redirect() throws Exception {
        mockMvc.perform(get("/user/index"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser(username = "thomas@gmail.com", password = "azerty", authorities = "ROLE_USER")
    void testGetIndexWithOAuth2User_OK() throws Exception {
        mockMvc.perform(get("/user/index").with(oauth2Login()
                        .attributes(attrs -> attrs.put("email", "thomas@gmail.com"))
                        .attributes(attrs -> attrs.put("id", "azerty"))))
                .andExpect(status().is2xxSuccessful());
    }
    @Test
    @WithMockUser(username = "thomas@gmail.com", password = "azerty", authorities = "ROLE_USER")
    void testGetIndexWithOidcUser_OK() throws Exception {
        mockMvc.perform(get("/user/index").with(oidcLogin()
                .userInfoToken(attrs -> attrs.email("thomas@gmail.com"))
                .idToken(attrs -> attrs.claim("id","azerty"))))
                .andExpect(status().is2xxSuccessful());
    }


    @Test
    @WithMockUser(username = "toto", password = "azerty", authorities = "ROLE_USER")
    void testGetIndex_OK() throws Exception {
        mockMvc.perform(get("/user/index"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "thomas@gmail.com", password = "azerty", authorities = "ROLE_USER")
    void testGetContact_OK() throws Exception {
        mockMvc.perform(get("/user/contact"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "thomas@gmail.com", password = "azerty", authorities = "ROLE_USER")
    void testPostContact_OK() throws Exception {
        String rodriguez = "rodriguez@gmail.com";
        mockMvc.perform(post("/user/contact").param("contactId", rodriguez).with(csrf()))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser(username = "thomas@gmail.com", password = "azerty", authorities = "ROLE_USER")
    void testAddContact_OK() throws Exception {
        String iban = "8096425203327472827974136135605065";
        String bic8 = "90747894";
        mockMvc.perform(post("/user/profile/addBankAccount").param("iban", iban).param("bic",bic8).with(csrf()))
                .andExpect(status().is2xxSuccessful());
    }
    @Test
    @WithMockUser(username = "thomas@gmail.com", password = "azerty", authorities = "ROLE_USER")
    void testGetProfile_OK() throws Exception {
        mockMvc.perform(get("/user/profile"))
                .andExpect(status().isOk());
    }

    @Test
    void testPostSignUp_OK() throws Exception {

        mockMvc.perform(post("/user/signup").param("username", "toto").param("password", "azerty").with(csrf()))
                .andExpect(status().isOk());
    }

}
