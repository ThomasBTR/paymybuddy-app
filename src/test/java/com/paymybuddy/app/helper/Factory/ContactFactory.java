package com.paymybuddy.app.helper.Factory;

import com.paymybuddy.app.database.models.Contact;
import com.paymybuddy.app.database.models.ContactPrimaryKey;

public class ContactFactory {


    public  static Contact createContact(){
        Contact contact = new Contact();
        ContactPrimaryKey contactPrimaryKey = new ContactPrimaryKey();
        contact.setId(contactPrimaryKey);
        return contact;
    }

    public static Contact createContact(ContactPrimaryKey contactPrimaryKey){
        Contact contact = createContact();
        contact.setId(contactPrimaryKey);
        return contact;
    }

}
