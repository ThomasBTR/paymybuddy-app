package com.paymybuddy.app.helper.Factory;

import com.paymybuddy.app.database.models.ContactPrimaryKey;
import com.paymybuddy.app.database.models.User;

public class ContactPrimaryKeyFactory {

    public static ContactPrimaryKey createCpk(User contactor){
        ContactPrimaryKey cpk = new ContactPrimaryKey();
        cpk.setContactorId(contactor);
        return cpk;
    }

    public static ContactPrimaryKey createCpk(User contactor, User contacted){
        ContactPrimaryKey cpk = createCpk(contactor);
        cpk.setContactedId(contacted);
        return cpk;
    }
}
