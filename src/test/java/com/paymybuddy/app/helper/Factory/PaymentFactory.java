package com.paymybuddy.app.helper.Factory;

import com.paymybuddy.app.database.models.Payment;
import com.paymybuddy.app.database.models.User;

import java.time.OffsetDateTime;

public class PaymentFactory {

    public static Payment createPayment(User creditor, User debtor){
        Payment p = new Payment();
        p.setCreditor(creditor);
        p.setDebtor(debtor);
        p.setDate(OffsetDateTime.now());
        p.setAmount(10f);
        p.setFees(0.05F);
        return p;
    }

    public static Payment createPayment(User creditor, User debtor,long id){
        Payment p = createPayment(creditor, debtor);
        p.setId(id);
        return p;
    }

    public static Payment createPayment(User creditor, User debtor,long id, float amount){
        Payment p = createPayment(creditor, debtor,id);
        p.setAmount(amount);
        return p;
    }

    public static Payment createPayment(User creditor, User debtor, Float amount){
        Payment p = createPayment(creditor, debtor);
        p.setAmount(amount);
        p.setDate(OffsetDateTime.now());
        return p;
    }
}
