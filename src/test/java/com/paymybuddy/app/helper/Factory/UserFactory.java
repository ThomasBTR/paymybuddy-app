package com.paymybuddy.app.helper.Factory;

import com.paymybuddy.app.database.models.BankAccount;
import com.paymybuddy.app.database.models.BankTransfert;
import com.paymybuddy.app.database.models.Payment;
import com.paymybuddy.app.database.models.User;

import java.util.ArrayList;

public class UserFactory {

    public static User createUser(String email, String password) {
        User user = new User();
        user.setId(email);
        user.setBalance(0f);
        user.setPassword(password);
        user.setContacts(new ArrayList<>());
        user.setBankAccounts(new ArrayList<>());
        user.setCreditorPayments(new ArrayList<>());
        user.setDebtorPayments(new ArrayList<>());
        user.setBankTransferts(new ArrayList<>());
        user.setAuthority("ROLE_USER");
        return user;
    }

    public static User createUser(String email, String password, String authority) {
        User user = createUser(email, password);
        user.setAuthority(authority);
        return user;
    }

    public static User createUser(String email, String password, BankAccount bankAccount) {
        User user = createUser(email, password);
        user.getBankAccounts().add(bankAccount);
        return user;
    }

    public static User createUser(String email, String password, BankAccount bankAccount, float balance) {
        User user = createUser(email, password, bankAccount);
        user.setBalance(balance);
        return user;
    }

    public static User createUser(String email, String password, BankAccount ba, User contact, User contactOf, Payment creditorPayment, Payment debtorPayment, BankTransfert bankTransfert) {
        User user = createUser(email, password);
        user.getBankAccounts().add(ba);
        user.getContacts().add(contact);
        user.getCreditorPayments().add(creditorPayment);
        user.getDebtorPayments().add(debtorPayment);
        user.getBankTransferts().add(bankTransfert);
        user.setAuthority("ROLE_USER");
        return user;
    }
}
