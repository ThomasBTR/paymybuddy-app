package com.paymybuddy.app.helper.Factory;

import com.paymybuddy.app.database.models.BankAccount;
import com.paymybuddy.app.database.models.BankTransfert;
import com.paymybuddy.app.database.models.User;
import com.paymybuddy.app.database.models.constants.TransfertType;

import java.time.OffsetDateTime;

public class BankTransfertFactory {

    public static BankTransfert createBankTransfert(){
        return new BankTransfert();
    }

    public static BankTransfert createBankTransfert(TransfertType transfertType, float amount, User ownerId, BankAccount bankAccount){
        BankTransfert bankTransfert = createBankTransfert();
        bankTransfert.setAmount(amount);
        bankTransfert.setTransfertType(transfertType);
        bankTransfert.setOwnerId(ownerId);
        bankTransfert.setDate(OffsetDateTime.now());
        bankTransfert.setBankAccount(bankAccount);
        return bankTransfert;
    }

    public static BankTransfert createBankTransfert(long id, TransfertType transfertType, float amount, User ownerId, BankAccount bankAccount){
        BankTransfert bankTransfert = createBankTransfert();
        bankTransfert.setId(id);
        bankTransfert.setAmount(amount);
        bankTransfert.setTransfertType(transfertType);
        bankTransfert.setOwnerId(ownerId);
        bankTransfert.setDate(OffsetDateTime.now());
        bankTransfert.setBankAccount(bankAccount);
        return bankTransfert;
    }

}
