package com.paymybuddy.app.helper.Factory;

import com.paymybuddy.app.database.models.BankAccount;
import com.paymybuddy.app.database.models.User;

import java.util.ArrayList;

public class BankAccountFactory {

    public static BankAccount createBankAccount(User owner){
     BankAccount ba = new BankAccount();
     ba.setId(1L);
     ba.setOwner(owner);
     ba.setBankTransferts(new ArrayList<>());
     ba.setBic("12345464");
     ba.setIban("1234567891234567891234567891234565");
     return ba;
    }

    public static BankAccount createBankAccount(User owner, String bic, String iban){
        BankAccount ba = createBankAccount(owner);
        ba.setIban(iban);
        ba.setBic(bic);
        return ba;
    }

    public static BankAccount createBankAccount(long id, User owner, String bic, String iban){
        BankAccount ba = createBankAccount(owner, bic, iban);
        ba.setId(id);
        return ba;
    }

}
