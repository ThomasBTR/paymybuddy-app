package com.paymybuddy.app;

import com.paymybuddy.app.configuration.SpringSecurityConfig;
import com.paymybuddy.app.database.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
class SpringWebAppTests {

    @Autowired
    private MockMvc mock;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    SpringSecurityConfig springSecurityConfig;

    @BeforeEach
    public void setup(){
        mock = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Autowired
    UserRepository userRepository;

    @Test
    void userLoginTest() throws Exception{
        String user = "thomas@gmail.com";
        String password = "azerty";
        mock.perform(formLogin("/login").user(user).password(password)).andExpect(authenticated());
    }

    @Test
    void userLoginFail() throws Exception{
        String user = "thomas@gmail.com";
        String password = "bezaertjtg";
        mock.perform(formLogin("/login").user(user).password(password)).andExpect(unauthenticated());
    }


}
