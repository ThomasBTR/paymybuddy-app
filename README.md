# PayMyBuddy app

Project 6 of OpenClassRooms Dev Java Training

##Getting Started

run the project with a command : ``mvn spring-boot:run``

Get tests results through : ``mvn clean verify`` 

Install the jar-with-dependencies with :  ``mvn clean install``

## Git Repo

The git repo is here : https://gitlab.com/ThomasBTR/paymybuddy-app

All the branches are up even though all of them have beeen merged on the main branch at the release.

You can check to all the pipelines passed (and failed) here : https://gitlab.com/ThomasBTR/paymybuddy-app/-/pipelines

## Coverage and Cie

Get all the infos on the current status of the project with SonarCloud :
https://sonarcloud.io/project/overview?id=ThomasBTR_paymybuddy-app

You can check each branches here : https://sonarcloud.io/project/branches_list?id=ThomasBTR_paymybuddy-app

## UML Diagram

![UML](/src/main/resources/MCD.PayMyBuddy.app.png)

This UML diagram show how models connection between user, bank account and payment.

 - User as several data, including id, password, balance, transaction revenue and a recursive connection with other users.
 - One to several bank accounts can be linked with a user, and a bank account object contains BIC and IBAN infos.
 - Two or several payments can be linked to a user, both money sent en received. Each payment as a unique id an specify the creditor and the debtor, the amount and the date of the transaction.
## Logical Data Model

![MLD](src/main/resources/MLD.en.PayMyBuddy.app.png)

In the LDM you find the UML adapted to a database level assuring we can create a database from that fit the requirements of our UML diagram.